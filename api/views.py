from rest_framework import generics
from .serializers import BookSerializer
from .models import Book


class Books(generics.ListCreateAPIView):
    queryset = Book.objects.all().order_by('title')
    serializer_class = BookSerializer


class Book(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
