from django.db import models


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=60)
    author = models.CharField(max_length=40)

    def __str__(self):
        return self.title 

    class Meta:
        ordering = ['title']
